# Debian-toolbox

[![pipeline status](https://gitlab.com/niklashh/debian-toolbox/badges/master/pipeline.svg)](https://gitlab.com/niklashh/debian-toolbox/-/commits/master)

This container can be used for running debian inside [toolbox](https://github.com/containers/toolbox).

```console
toolbox create --image registry.gitlab.com/niklashh/debian-toolbox:latest
toolbox enter debian-toolbox
```

## Building manually

To build the toolbox container

```console
./build.sh
```

To start the toolbox

```console
toolbox create --image localhost/debian-toolbox
toolbox enter debian-toolbox
```

To remove the toolbox

```console
toolbox rm -f debian-toolbox
```

## License

MIT
